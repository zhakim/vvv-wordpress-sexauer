#!/usr/bin/env bash
# Provision WordPress Stable
# v1.0

set -x
trap read debug

# Edit these variables to suit your porpoises
# -------------------------------------------

# Just a human readable description of this site
SITE_NAME="Sexauer Gallery"
# The name (to be) used by MySQL for the DB
# DB_NAME="wp"
# The repo URL in SSH format, e.g. git@bitbucket.org:zhakim/wp.git
REPO_SSH_URL="git@bitbucket.org:zhakim/wp.git"
# The multisite stuff for wp-config.php
EXTRA_CONFIG="
// No extra config, but if there was multisite stuff, etc,
// it would go here.
"

# backup restore info
# BACKUP_DIR=${VVV_PATH_TO_SITE}/my_backup
BACKUP_DIR=${VVV_PATH_TO_SITE}/../my_backup
WWW_DIR=${VVV_PATH_TO_SITE}/public_html

DB_USER=wp
DB_PASS=wp
DB_NAME=wp
DB_HOST=localhost
RESTORE_DB=true
RESTORE_FILE=true

DOMAIN_OLD="sexauer.eu"
DOMAIN_NEW="sexauer.dev"

#color consul output
RED='\e[0;31m'
GREEN='\e[0;32m'
NC='\e[0m' # No Color

echo "---------------------------"
echo -e "Hi Sami!!! \n\n\nCommencing $SITE_NAME setup"

# Nginx Logs
mkdir -p ${VVV_PATH_TO_SITE}/log
touch ${VVV_PATH_TO_SITE}/log/error.log
touch ${VVV_PATH_TO_SITE}/log/access.log

# make a backup dir

mkdir -p $BACKUP_DIR

# repo and wordpress
if [[ ! -d "${VVV_PATH_TO_SITE}/public_html" ]]; then


	mkdir -p ${VVV_PATH_TO_SITE}/public_html

	echo -e "\n ${RED}You have to download the repo:\n${NC}"
	echo -e "${NC}cd ${VVV_PATH_TO_SITE} ${NC}"
	echo -e "${NC}git clone $REPO_SSH_URL public_html${NC}"
	echo -e "¢{GREEN}or run:"
	echo -e "${NC}cd /Users/ziadhakim/Documents/Vagrant/VVV/www/sexauer-dev && git clone $REPO_SSH_URL public_html"
	echo -e "${NC}when finish run again 'vagrant reload --provision'${NC}\n"
	echo -e "${GREEN}Bye Bye!\n\n${NC}"	
  
else

	if [[ ! -f "${VVV_PATH_TO_SITE}/public_html/wp-config.php" ]]; then
		echo -e "\n${RED}You have to download the repo first${NC}"
		echo -e "${NC}cd ${VVV_PATH_TO_SITE} ${NC}"
		echo -e "${NC}git clone $REPO_SSH_URL public_html${NC}"
		echo -e "¢{GREEN}or run:"
		echo -e "${NC}cd /Users/ziadhakim/Documents/Vagrant/VVV/www/sexauer-dev && git clone $REPO_SSH_URL public_html"
		echo -e "${NC}when finish run again 'vagrant reload --provision'${NC}\n"
		echo -e "${GREEN}Bye Bye!\n\n${NC}"
		exit 1
	fi

	# Make a database, if we don't already have one
	# mysql -u root --password=root -e "DROP DATABASE IF EXISTS $DB_NAME"
	mysql -u root --password=root -e "CREATE DATABASE IF NOT EXISTS $DB_NAME"
	mysql -u root --password=root -e "GRANT ALL PRIVILEGES ON $DB_NAME.* TO wp@localhost IDENTIFIED BY 'wp';"
	echo -e "\n ${GREEN}DB operations done.\n\n${NC}"

	if [[ ! -d "${VVV_PATH_TO_SITE}/public_html/wordpress" ]]; then

		cd ${VVV_PATH_TO_SITE}/public_html
		# 1- Download the latest stable version of WordPress
  		echo "Downloading WordPress Stable, see http://wordpress.org/"
  		curl -L -O "https://wordpress.org/latest.tar.gz"
  		noroot tar -xvf latest.tar.gz
  		rm latest.tar.gz

  # 		echo "Installing WordPress Stable..."
		# noroot wp core install --url=sexauer.dev --quiet --title="Sexauer Galley Dev" --admin_name=admin --admin_email="admin@local.dev" --admin_password="password"
	else

		echo "Updating WordPress Stable..."
  		# cd ${VVV_PATH_TO_SITE}/public_html/wordpress
  		cd ${VVV_PATH_TO_SITE}/public_html
  		noroot wp core update

  # 		if ! $(wp core is-installed); then
  # 			echo -e "\n ${GREEN}Installing wordpress after update.\n${NC}"
  #   		noroot wp core install --url=sexauer.dev --quiet --title="Sexauer Galley Dev" --admin_name=admin --admin_email="admin@local.dev" --admin_password="password"
		# fi
	fi

	
	if [[ $RESTORE_FILE == true || $RESTORE_DB == true ]]; then

		echo -e "${GREEN}restore wordpress db and data from backup"

		if [ "$(ls -A $BACKUP_DIR)" ]; then
  		   
  		   echo "Take action $BACKUP_DIR is not Empty"
  		   cd ${VVV_PATH_TO_SITE}/provision
			bash restore.sh "$BACKUP_DIR" "$WWW_DIR" "$DB_NAME" "$DB_USER" "$DB_PASS" "$DB_HOST" "$RESTORE_DB" "$RESTORE_FILE"

			if [[ $RESTORE_DB == true ]]; then
				echo "search and replace hostname in the Database"
				cd ${VVV_PATH_TO_SITE}/../Search-Replace-DB-master
				php srdb.cli.php -h $DB_HOST -n $DB_NAME -u $DB_USER -p $DB_PASS -s $DOMAIN_OLD -r $DOMAIN_NEW 
				echo "END search and replace-DB"
			fi
			
		else
    		echo "$BACKUP_DIR is Empty"
		fi

		
	else
		echo -e "${GREEN}NOT gonna restore."
	fi

	
fi

# The Vagrant site setup script will restart Nginx for us

echo -e "${GREEN}${SITE_NAME} ZIAD init is complete${NC}"

set +x



