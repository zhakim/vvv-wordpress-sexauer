#!/bin/bash

# # Set the date format, filename and the directories where your backup files will be placed and which directory will be archived.
# #DATE=$(date +"%Y-%m-%d-%Hh%Mm")
# PROJECT_NAME="wp1_local"
# #FILE="wp1.local.$NOW.tar"
# BACKUP_DIR=/Users/ziad/Projects/backups/$PROJECT_NAME
# #WPCONTENT-DIR="/Users/ziad/Projects/Sites/wp1.local/content"
# WWW_DIR="/Users/ziad/Projects/Sites/wp1.local/content"

# # MySQL database credentials
# DB_USER=wp1
# DB_PASS=password
# DB_NAME=wp1
# DB_HOST=localhost
# #DB_FILE="wp1.local.$NOW.sql"

# #*****************************************
# #Location of mysqldump command. Here you need to substitute the �path_to_mysqldump�
# #with the path to the location where the mysqldump command is situated.
# #E.g. /usr/local/mysql-5.0.51a-freebsd7.0-i386/bin/
# MYSQLDUMPLOC="/Applications/MAMP/Library/bin/"

# #GO
# add mysqldump to path
# PATH=$PATH:$MYSQLDUMPLOC;
# #cd $BACKUP_DIR

set -x
trap read debug

BACKUP_DIR=$1
WWW_DIR=$2
DB_NAME=$3
DB_USER=$4
DB_PASS=$5
DB_HOST=$6
RESTORE_DB=$7
RESTORE_FILE=$8
PROJECT_NAME="sexauer_eu"



echo "----------------------------------------------------"
echo "Existing backups:"

for entry in "$BACKUP_DIR"/*
do
  # if [ -f "$entry" ];then
  if [ -d "$entry" ];then
    dirname="$entry"
    echo "$dirname"
 fi
done

# echo "Type the backup directory name (without the absalute path) you want to restor, followed by [ENTER]:"
# read dirname



# mkdir $BACKUP_DIR/$dirname/temp
mkdir $dirname/temp


# for entry in "$BACKUP_DIR/$dirname"/*
for entry in "$dirname"/*
do
 	if [ -f "$entry" ];then
    	# cp $entry $BACKUP_DIR/$dirname/temp
      cp $entry $dirname/temp
 	fi
done


# for entry in $BACKUP_DIR/$dirname/temp/*
for entry in $dirname/temp/*
do
    gunzip $entry
done

# save the files name
for file in $dirname/temp/*
do
  if [[ ${file: -4} == ".sql" ]]; then
    sqlFile=$file
  elif [[ ${file: -4} == ".tar" ]]; then
      tarFile=$file
  fi

done

echo "--------"
echo $tarFile
echo $sqlFile

# echo "Are you sure you want to restor the files? [Y/n] "
# read answer
if [[ $RESTORE_FILE == true ]]; then
  echo "gonna to restor the files"
  # tar -xpf $BACKUP_DIR/$dirname/temp/$PROJECT_NAME-$dirname.tar -C $BACKUP_DIR/$dirname/temp/
  tar -xpf $tarFile -C $dirname/temp/
	# rm -r $WWW_DIR/*
	cp -pr $dirname/temp/_wp/content $WWW_DIR/
else
	echo "not gonna restore the files"
fi


if [[ $RESTORE_DB == true ]]; then
  echo "gonna to restor the Database"
  mysql -h "$DB_HOST" -u "$DB_USER" -p "$DB_PASS" "$DB_NAME" < $sqlFile
else
	echo "you do not want to restore the database"
fi


rm -rf $dirname/temp

echo "DONE!!!!" 

